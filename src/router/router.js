import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Login from '@/views/Login.vue';
import Post from '@/views/Post.vue';
/* eslint-disable import/extensions */
import store from '@/store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
    },
    {
      path: '/post/:id',
      name: 'post',
      component: Post,
    },
  ],
});

router.beforeEach((to, from, next) => {
  const loggedIn = store.getters['isLoggedIn'];
  console.log(loggedIn)

  if (!loggedIn && to.name === 'home') {
    next({
      path: '/',
      // query: { redirect: to.fullPath },
    });
  }

  // if (loggedIn) {
  //   next({
  //     path: '/home',
  //   });
  // }

  next();
});

export default router;
