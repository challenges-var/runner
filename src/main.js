// Element UI
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// Bootstrap Vue
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
// Vue
import Vue from 'vue';
import router from './router/router';
import store from './store';
import './registerServiceWorker';
// App
import App from './App.vue';
import './styles/main.scss';


Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(Element);


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
