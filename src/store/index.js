import Vue from 'vue';
import Vuex from 'vuex';
import { fetchData, fetchPost } from '@/api';
/* eslint-disable no-param-reassign */

const LOGIN = 'LOGIN';
const LOGIN_SUCCESS = 'LOGIN_SUCCES';
const LOGOUT = 'LOGOUT';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoggedIn: !!localStorage.getItem('token'),
    data: [],
  },
  getters: {
    isLoggedIn: state => state.isLoggedIn,
  },
  actions: {
    login({ commit }) {
      commit(LOGIN);
      return new Promise((resolve) => {
        setTimeout(() => {
          localStorage.setItem('token', 'JWT');
          commit(LOGIN_SUCCESS);
          console.log('Logged In');
          resolve();
        }, 1000);
      });
    },
    logout({ commit }) {
      localStorage.removeItem('token');
      commit(LOGOUT);
      console.log('Logged Out');
    },
    getPosts({ commit }) {
      return new Promise((resolve, reject) => {
        fetchData()
          .then((response) => {
            const endpoint = response.data.posts;

            commit('SET_POSTS', endpoint);
            resolve(response);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },

    getPost({ commit }, id) {
      return new Promise((resolve, reject) => {
        fetchPost(id)
          .then((response) => {
            const endpoint = response.data.posts;

            commit('SET_POSTS', endpoint);
            resolve(response);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },
  },
  mutations: {
    [LOGIN](state) {
      state.pending = true;
    },
    [LOGIN_SUCCESS](state) {
      state.isLoggedIn = true;
      state.pending = false;
    },
    [LOGOUT](state) {
      state.isLoggedIn = false;
    },
    SET_POSTS(state, data) {
      Vue.set(state, 'data', data);
    },
  },
});
