import { HTTP } from './http';

/* eslint-disable */
export const fetchData = () => new Promise((resolve, reject) => HTTP.get(`https://jsonplaceholder.typicode.com/posts/`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));

export const fetchPost = (id) => new Promise((resolve, reject) => HTTP.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));
